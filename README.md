# MULTICAST SENDER

## PURPOSE
This little python program was created to simply send 2 packets with a desired layer 2 multicast destination through a desired interface.

## DISCLAIMER
Has been tested on simple switches and regular cisco switches **ONLY**. So frames could be dropped by other network elements if they look at other things inside the packet.

## DEPENDENCIES
You must have the **python-pip** and the python module **scapy** installed
### how to install pip ?
`sudo apt-get install python-pip`
### how to install scapy ?
`pip install scapy`

## HOW TO USE IT
The program takes many mandatory arguments :
### The source interface : -s 
The source network interface name of the sender
### The target multicast : -a or -d 
-a means for all multicast addresses present in the addresses.ods file (NOT IMPLEMENTED YET)
-d means a specific multicast address (you must give it)
### The Ether Type of the frame : -t
When using the *-d* option, please add the corresponding ether type (see addresses.ods file)

## EXEMPLES
`sudo python MulticastSender.py -h` or `python MulticastSender.py -h`
-> print the help 

`sudo python MulticastSender.py -s eno1 -d 33:33:00:00:00:00 -t 86DD`
-> send 2 packets to the IPv6 Neighbor discovery Multicast Address from our eno1 interface



> Please refer to the given help for more explanations.

### **(** 
or with me directly <mailto:loic.bassang@hefr.ch> 
### **)**