import sys, getopt, fcntl, socket, struct
from scapy.all import * 

def getHwAddr(ifname):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    info = fcntl.ioctl(s.fileno(), 0x8927,  struct.pack('256s', ifname[:15]))
    return ':'.join(['%02x' % ord(char) for char in info[18:24]])

isAll = 0
dst_addr = "FF:FF:FF:FF:FF:FF"
src_int = "eno1"
packet = 0
ether_type = 0

def preparePacket():
	global ether_type
	global src_int
	global packet
	global dst_addr
	ether = Ether()
	ether.dst = dst_addr
	ether.type = int(ether_type,16)
	data = "hello"
	ether.src = getHwAddr(src_int)
	packet = ether/data
	packet.show()
	sendp(packet, count=2, iface=src_int)

def usage():
	print "Multicast Sender\n"
	print "Autor: Bassang Loic\n"
	print "Usage :\n"
	print "Send paquet on selected multicast addresses\n"
	print "\t-h or --help                  print this help\n"
	print "\t-a or --all                   send multicast to all pre-defined address\n"
	print "\t-d [MAC_ADDR] or\n"
	print "\t--mac-dst [MAC_ADDR]          send multicast to the specified MAC_ADDR\n"
	print "\t-s [INT_SRC] or\n"
	print "\t --interface-source [INT_SRC] select the specified source interface\n"
	print "\t-t [ETHER_TYPE]               define the ether proto type of the paquet\n"


def parse(argv):
	try:
		opts, args = getopt.getopt(argv, "had:s:t:", ["help", "all", "mac-dst=", "interface-source="])
	except getopt.GetoptError:
		usage()
		sys.exit(2)
	for opt, arg in opts:
		if opt in ("-h", "--help"):
			print "OPTION H\n"
			usage()
			sys.exit()
		elif opt in ("-a", "--all"):
			print "OPTION A\n"
			print "All destination\n"
			global isAll
			isAll = 1
		elif opt in ("-d", "--mac-dst"):
			print "OPTION D\n"
			global dst_addr
			dst_addr = arg
			print "Destination is %s\n" % (dst_addr)
		elif opt in ("-s", "--interface-source"):
			print "OPTION S\n"
			global src_int
			src_int = arg
			print "Interface source is %s\n" % (src_int)
		elif opt in ("-t"):
			print "OPTION T\n"
			global ether_type
			ether_type = arg
			print "Ether proto type is %s\n" % (ether_type)

if __name__ == "__main__":
	if len(sys.argv) == 1:
		usage()
		sys.exit(2)
	parse(sys.argv[1:])
	preparePacket()
